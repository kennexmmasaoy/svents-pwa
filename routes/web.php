<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/','HomepageController@index');
Route::get('/blog','HomepageController@blog');
Route::get('/contact','HomepageController@contact');
Route::get('/services','HomepageController@services');
Route::get('/about','HomepageController@about');


Route::get('admin/login','AdminController@login');
Route::get('admin/dashboard','AdminController@dashboard');
Route::get('admin/user','AdminController@user');