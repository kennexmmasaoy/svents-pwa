<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomepageController extends Controller
{
    public function index(){
        return view('homepage.index');
    }
    public function blog(){
        return view('homepage.blog');
    }
    public function about(){
        return view('homepage.about');
    }
    public function services(){
        return view('homepage.services');
    }
    public function contact(){
        return view('homepage.contact');
    }
}
