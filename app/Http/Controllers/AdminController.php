<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AdminController extends Controller
{
    public function login(){
        return view('login');
    }
    public function dashboard(){
        return view('admin.index');
    }
    public function user(){
        return view('admin.user');
    }
}
