@extends('layouts.homepage')
@section('content')
<div id="colorlib-main">
			<div class="colorlib-services">
				<div class="container-fluid">
					<div class="row">
						<div class="col-md-6 col-md-offset-3 col-md-pull-3">
							<span class="heading-meta">What I do?</span>
							<h2 class="colorlib-heading animate-box" data-animate-effect="fadeInLeft">Here are some of my expertise</h2>
						</div>
					</div>
					<div class="row">
						<div class="col-md-6">
							<div class="colorlib-feature animate-box" data-animate-effect="fadeInLeft">
								<div class="colorlib-icon">
									<i class="icon-tag"></i>
								</div>
								<div class="colorlib-text">
									<h3>Branding</h3>
									<p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. </p>
								</div>
							</div>

							<div class="colorlib-feature animate-box" data-animate-effect="fadeInLeft">
								<div class="colorlib-icon">
									<i class="icon-pen2"></i>
								</div>
								<div class="colorlib-text">
									<h3>Web Design</h3>
									<p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. </p>
								</div>
							</div>

							<div class="colorlib-feature animate-box" data-animate-effect="fadeInLeft">
								<div class="colorlib-icon">
									<i class="icon-search3"></i>
								</div>
								<div class="colorlib-text">
									<h3>Search engine optimization</h3>
									<p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. </p>
								</div>
							</div>
						</div>
						<div class="col-md-6">
							<div class="colorlib-feature animate-box" data-animate-effect="fadeInLeft">
								<div class="colorlib-icon">
									<i class="icon-paperplane"></i>
								</div>
								<div class="colorlib-text">
									<h3>Web Development</h3>
									<p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. </p>
								</div>
							</div>

							<div class="colorlib-feature animate-box" data-animate-effect="fadeInLeft">
								<div class="colorlib-icon">
									<i class="icon-device-desktop"></i>
								</div>
								<div class="colorlib-text">
									<h3>User Interface</h3>
									<p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. </p>
								</div>
							</div>

							<div class="colorlib-feature animate-box" data-animate-effect="fadeInLeft">
								<div class="colorlib-icon">
									<i class="icon-help"></i>
								</div>
								<div class="colorlib-text">
									<h3>Help &amp; Support</h3>
									<p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. </p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>


			<div class="colorlib-work">
				<div class="container-fluid">
					<div class="row">
						<div class="col-md-6 col-md-offset-3 col-md-pull-3">
							<span class="heading-meta">Portfolio</span>
							<h2 class="colorlib-heading animate-box" data-animate-effect="fadeInLeft">Recent Work</h2>
						</div>
					</div>
					<div class="row">
						<div class="col-md-6 animate-box" data-animate-effect="fadeInLeft">
							<div class="project" style="background-image: url(images/work-1.jpg);">
								<div class="desc">
									<div class="con">
										<h3><a href="#">Work 01</a></h3>
										<span>Branding, Ilustration</span>
										<p class="icon">
											<span><a href="#"><i class="icon-share3"></i></a></span>
											<span><a href="#"><i class="icon-eye"></i> 100</a></span>
											<span><a href="#"><i class="icon-heart"></i> 49</a></span>
										</p>
									</div>
								</div>
							</div>
						</div>
						<div class="col-md-6 animate-box" data-animate-effect="fadeInLeft">
							<div class="project" style="background-image: url(images/work-2.jpg);">
								<div class="desc">
									<div class="con">
										<h3><a href="work.html">Work 02</a></h3>
										<span>Logo, Web, Branding</span>
										<p class="icon">
											<span><a href="#"><i class="icon-share3"></i></a></span>
											<span><a href="#"><i class="icon-eye"></i> 100</a></span>
											<span><a href="#"><i class="icon-heart"></i> 49</a></span>
										</p>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div id="get-in-touch" class="colorlib-bg-color">
				<div class="colorlib-narrow-content">
					<div class="row">
						<div class="col-md-6 animate-box" data-animate-effect="fadeInLeft">
							<h2>Get in Touch!</h2>
						</div>
					</div>
					<div class="row">
						<div class="col-md-6 col-md-offset-3 col-md-pull-3 animate-box" data-animate-effect="fadeInLeft">
							<p class="colorlib-lead">Separated they live in Bookmarksgrove right at the coast of the Semantics, a large language ocean.</p>
							<p><a href="#" class="btn btn-primary btn-learn">Contact me!</a></p>
						</div>
						
					</div>
				</div>
			</div>
		</div>
@endsection