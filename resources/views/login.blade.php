<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<title>Login - Bootstrap Admin Template</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
		<meta name="apple-mobile-web-app-capable" content="yes"> 	
		<link href="{{asset('assets-admin/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css" />
		<link href="{{asset('assets-admin/css/bootstrap-responsive.min.css')}}" rel="stylesheet" type="text/css" />
		<link href="{{asset('assets-admin/css/font-awesome.css')}}" rel="stylesheet">
		<link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
		<link href="{{asset('assets-admin/css/style.css')}}" rel="stylesheet" type="text/css">
		<link href="{{asset('assets-admin/css/pages/signin.css')}}" rel="stylesheet" type="text/css">

	</head>
	<body>
		<div class="account-container">
			<div class="content clearfix">
				<form action="#" method="post">
					<h1>Administrator Login</h1>	
					<div class="login-fields">
						<p>Please provide your details</p>
						<div class="field">
							<label for="username">Username</label>
							<input type="text" id="username" name="username" value="" placeholder="Username" class="login username-field" />
						</div> <!-- /field -->
						<div class="field">
							<label for="password">Password:</label>
							<input type="password" id="password" name="password" value="" placeholder="Password" class="login password-field"/>
						</div> <!-- /password -->
					</div> <!-- /login-fields -->
					<div class="login-actions">
						<button class="button btn btn-success btn-large">Sign In</button>
					</div> <!-- .actions -->
				</form>
			</div> <!-- /content -->
		</div> <!-- /account-container -->
		<script src="{{asset('assets-admin/js/jquery-1.7.2.min.js')}}"></script>
		<script src="{{asset('assets-admin/js/bootstrap.js')}}"></script>
		<script src="{{asset('assets-admin/js/signin.js')}}"></script>
	</body>

</html>
